# Calling gained and lost genes in human, mouse and opossum single cell cerebellum data
## Info
The code presented here belongs to the [mammalian cerebellum](https://gitlab.com/kleiss/mammalian-cerebellum) project.

Input data is available upon request.


## Running the code 

Classification of the three-way orthologs into the different gain and loss groups is done using:
```bash
Rscript --vanilla \
    scripts/call_gains_losses.R
```

To investigate the influence of the V2 and V3 10x Chromium versions on the calls,
the following script is used.
This script evaluates what happens, if calls were made on V2 exclusively, and a single V3 sample is 
added to the human dataset:

```bash
Rscript --vanilla \
 scripts/presence_absence_v3_influence.R
```

To test, whether the species specific difference in exonic lengths can explain the made calls,
a [python notebook](./scripts/notebooks/gain_loss_exonic_context_prediction.ipynb) was created, that trains a random forrest model on the classes and exonic properties.


The calls are verified using the published data of developmental bulk RNAseq ([Cardoso-Moreira et al.](https://www.nature.com/articles/s41586-019-1338-5)).

```bash
Rscript --vanilla \
 scripts/bulk_verification.R
```

Disease associations between the called classes is done using:

```bash
Rscript --vanilla \
 scripts/disease_associations.R
```

## Session Info
```
R version 3.6.3 (2020-02-29)
Platform: x86_64-pc-linux-gnu (64-bit)
Running under: Ubuntu 18.04.6 LTS

Matrix products: default
BLAS:   /usr/lib/x86_64-linux-gnu/openblas/libblas.so.3
LAPACK: /usr/lib/x86_64-linux-gnu/libopenblasp-r0.2.20.so

locale:
 [1] LC_CTYPE=en_US.UTF-8       LC_NUMERIC=C               LC_TIME=en_US.UTF-8       
 [4] LC_COLLATE=en_US.UTF-8     LC_MONETARY=en_US.UTF-8    LC_MESSAGES=en_US.UTF-8   
 [7] LC_PAPER=en_US.UTF-8       LC_NAME=C                  LC_ADDRESS=C              
[10] LC_TELEPHONE=C             LC_MEASUREMENT=en_US.UTF-8 LC_IDENTIFICATION=C       

attached base packages:
[1] parallel  stats4    stats     graphics  grDevices utils     datasets  methods  
[9] base     

other attached packages:
 [1] forcats_0.4.0               stringr_1.4.0               dplyr_0.8.3                
 [4] purrr_0.3.3                 readr_1.3.1                 tidyr_1.0.0                
 [7] tibble_2.1.3                ggplot2_3.3.2               tidyverse_1.3.0            
[10] SingleCellExperiment_1.6.0  SummarizedExperiment_1.14.0 DelayedArray_0.10.0        
[13] BiocParallel_1.18.0         matrixStats_0.56.0          Biobase_2.44.0             
[16] GenomicRanges_1.36.0        GenomeInfoDb_1.20.0         IRanges_2.18.0             
[19] S4Vectors_0.22.0            BiocGenerics_0.30.0        

loaded via a namespace (and not attached):
 [1] Rcpp_1.0.5             lubridate_1.7.4        lattice_0.20-45       
 [4] digest_0.6.18          utf8_1.1.4             assertthat_0.2.1      
 [7] plyr_1.8.4             R6_2.4.1               cellranger_1.1.0      
[10] backports_1.1.4        reprex_0.3.0           httr_1.4.2            
[13] pillar_1.4.3           zlibbioc_1.30.0        rlang_0.4.10          
[16] readxl_1.3.1           rstudioapi_0.13        Matrix_1.5-1          
[19] labeling_0.3           RCurl_1.98-1.2         munsell_0.5.0         
[22] broom_0.5.2            compiler_3.6.3         modelr_0.1.5          
[25] pkgconfig_2.0.3        tidyselect_0.2.5       GenomeInfoDbData_1.2.1
[28] fansi_0.4.0            crayon_1.4.1           dbplyr_1.4.2          
[31] withr_2.4.1            bitops_1.0-6           grid_3.6.3            
[34] nlme_3.1-162           jsonlite_1.7.2         gtable_0.3.0          
[37] lifecycle_1.0.0        DBI_1.1.1              magrittr_2.0.1        
[40] scales_1.1.1           cli_2.3.1              stringi_1.4.3         
[43] farver_2.0.3           reshape2_1.4.3         XVector_0.24.0        
[46] fs_1.3.1               xml2_1.3.2             ellipsis_0.3.2        
[49] vctrs_0.3.6            generics_0.0.2         tools_3.6.3           
[52] glue_1.4.1.9000        hms_1.0.0              colorspace_1.4-1      
[55] rvest_1.0.0            haven_2.3.1        
```

# Citation
```bibtex
@article {Sepp2021.12.20.473443,
	author = {Mari Sepp and Kevin Leiss and Ioannis Sarropoulos and Florent Murat and Konstantin Okonechnikov and Piyush Joshi and Evgeny Leushkin and Noe Mbengue and C{\'e}line Schneider and Julia Schmidt and Nils Trost and Lisa Sp{\"a}nig and Peter Giere and Philipp Khaitovich and Steven Lisgo and Mikl{\'o}s Palkovits and Lena M. Kutscher and Simon Anders and Margarida Cardoso-Moreira and Stefan M. Pfister and Henrik Kaessmann},
	title = {Cellular development and evolution of the mammalian cerebellum},
	elocation-id = {2021.12.20.473443},
	year = {2021},
	doi = {10.1101/2021.12.20.473443},
	publisher = {Cold Spring Harbor Laboratory},
	abstract = {The expansion of the neocortex, one of the hallmarks of mammalian evolution1,2, was accompanied by an increase in the number of cerebellar neurons3. However, little is known about the evolution of the cellular programs underlying cerebellum development in mammals. In this study, we generated single-nucleus RNA-sequencing data for \~{}400,000 cells to trace the development of the cerebellum from early neurogenesis to adulthood in human, mouse, and the marsupial opossum. Our cross-species analyses revealed that the cellular composition and differentiation dynamics throughout cerebellum development are largely conserved, except for human Purkinje cells. Global transcriptome profiles, conserved cell state markers, and gene expression trajectories across neuronal differentiation show that the cerebellar cell type-defining programs have been overall preserved for at least 160 million years. However, we also discovered differences. We identified 3,586 genes that either gained or lost expression in cerebellar cells in one of the species, and 541 genes that evolved new expression trajectories during neuronal differentiation. The potential functional relevance of these cross-species differences is highlighted by the diverged expression patterns of several human disease-associated genes. Altogether, our study reveals shared and lineage-specific programs governing the cellular development of the mammalian cerebellum, and expands our understanding of the evolution of mammalian organ development.Competing Interest StatementThe authors have declared no competing interest.},
	URL = {https://www.biorxiv.org/content/early/2021/12/21/2021.12.20.473443},
	eprint = {https://www.biorxiv.org/content/early/2021/12/21/2021.12.20.473443.full.pdf},
	journal = {bioRxiv}
}
```