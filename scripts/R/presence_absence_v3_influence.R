library(tidyverse)

df <- read_csv('output/o2o_pseudobulks.csv')


head(df)

# only looking at mouse and human
df <- df %>% 
  filter(species %in% c('HUM', 'MOU'))

# then, I filter for only Chromium version 2
dfv2 <- df %>% 
  filter(chrver == 'Chromium') %>% 
  group_by(cell_type, stage) %>% 
  filter(length(unique(species)) == 2) %>% 
  mutate(ct_stage = paste0(cell_type, '-', stage)) %>% 
  ungroup()

# for later supplementation, I create a human v3 data frame
dfhv3 <- df %>% 
  filter(chrver == 'Chromium_v3') %>% 
  filter(species == 'HUM') %>% 
  mutate(ct_stage = paste0(cell_type, '-', stage)) %>% 
  filter(ct_stage %in% unique(dfv2$ct_stage)) %>% 
  ungroup()

set.seed(1234)
v3tid <- sample(x = unique(dfhv3$tid), size = 1)
cat("keeping human v3 sample:", v3tid, '\n')

dfhv3_single <- dfhv3 %>% 
  filter(tid == v3tid)

set.seed(1234)
v2rem <- dfv2 %>% 
  filter(species == 'HUM') %>% 
  pull(tid) %>% 
  unique() %>% 
  sample(size =1)
cat("removing human v2 sample:", v2rem, '\n')

dfv2rem <- dfv2 %>% 
  filter(tid != v2rem)

dfv2v3 <- rbind(dfv2rem, dfhv3_single)
## Parameters ------------------------------------------------------------------

subtype_exclude <- c(
  'progenitor_MB',
  'interneuron_Meis2'
)

subtype_include <- c(
  'microglia'
)

prop_percentile_cutoff <- 0.25
exon_length_gc_cutoff_log2 <- 1.5
evodevo.rpkm.cutoff <- 5
detection_cutoff <- 50  #cpm
background_cutoff <- 20  #cpm
n_above_cutoff <- 2
min_fold_difference <- 5


## Reading evodevo data and metadata -------------------------------------------

species.evodevo <- c(
  MOU = 'Mouse',
  HUM = 'Human'
)

gns_meta_full <- read_csv("data/metadata/all_genes_meta.csv") %>%
  filter(ortho_type == 'ortholog_one2one') %>%
  filter(!str_detect(gene_name, 'MT-')) %>%
  filter(!str_detect(gene_name, 'mt-'))

gns_meta_ortho <- gns_meta_full
gns_meta <- gns_meta_full %>%
  select(gene_id, gene_name) %>%
  distinct()

matched_tp <- read_csv('data/metadata/stage_matching_colors_long.csv')
# evodevo data -----------------------------------------------------------------
evodevo <- lapply(species.evodevo, function(s){
  tmp <- read_delim(
    paste0('data/metadata/bulk/Plotting_expression/',s,'RpkmMajorTissuesCor90.Norm.txt'),
    delim = ' '
  )
  tmp <- tmp %>% filter(gene_id %in% gns_meta_ortho$gene_id)

  tmp %>%
    mutate(gene_id = gns_meta_ortho$ortho_id[match(gene_id, gns_meta_ortho$gene_id)]) %>%
    select(gene_id, contains('Cerebellum')) %>%
    gather('bulk_sample', 'rpkm', contains('Cerebellum')) %>%
    add_column(species = names(species.evodevo[species.evodevo == s])) %>%
    filter(rpkm > evodevo.rpkm.cutoff)
}) %>%
  do.call(rbind,.)

# exon quantification table ----------------------------------------------------
exon_quant <- lapply(
  c('human', 'mouse'),
  function(s){
    read_csv(glue::glue('data/metadata/{s}_exon_quantification.csv')) %>%
      add_column(species = s)
  }
) %>% do.call(rbind,.) %>% 
  mutate(species = recode(species,
                          mouse = "MOU",
                          opossum = "OPO",
                          human = "HUM")) %>% 
  filter(gene_id %in% gns_meta_full$gene_id) %>% 
  left_join(gns_meta_full %>% select(gene_id, ortho_id, species)) %>% 
  select(-gene_id) %>% 
  dplyr::rename(gene_id = ortho_id)

exon_quant_length <- exon_quant %>% 
  select(-gc) %>% 
  spread(species, length) %>% 
  mutate(length_diff_hm = log2(HUM / MOU)) %>% 
  drop_na() %>% 
  dplyr::rename(exon_length_HUM = HUM,
                exon_length_MOU = MOU) %>% 
  mutate(exon_length_class = case_when(
    abs(length_diff_hm) < exon_length_gc_cutoff_log2 ~ 'comparable',
    TRUE ~ 'exclude'
  ))

exon_quant_gc <- exon_quant %>% 
  select(-length) %>% 
  spread(species, gc) %>% 
  mutate(gc_diff_hm = log2(HUM / MOU)) %>% 
  drop_na() %>% 
  dplyr::rename(exon_gc_HUM = HUM,
                exon_gc_MOU = MOU) %>% 
  mutate(exon_gc_class = case_when(
    abs(gc_diff_hm) < exon_length_gc_cutoff_log2 ~ 'comparable',
    TRUE ~ 'exclude'
  ))

exdiff <- read_csv('data/exon_lengths_HumMouOpo.csv') %>% 
  mutate(exon_length_class = case_when(
    abs(exon_diff_hm_medianAdj_log2) < exon_length_gc_cutoff_log2 &
      abs(exon_diff_ho_medianAdj_log2) < exon_length_gc_cutoff_log2 &
      abs(exon_diff_mo_medianAdj_log2) < exon_length_gc_cutoff_log2 ~ 'comparable',
    TRUE ~ 'exclude'
  ))



exon_classification <- left_join(
  exdiff,
  exon_quant_gc
) %>% 
  mutate(exon_conservation_class = case_when(
    exon_length_class == 'comparable' & exon_gc_class == 'comparable' ~ 'comparable',
    TRUE ~ 'exclude'
  ))

# getting maximum expression per species --------------------------------------
evodevo.max <- evodevo %>%
  group_by(gene_id, species) %>%
  top_n(1, rpkm) %>%
  sample_n(1) %>%
  select(-bulk_sample) %>%
  spread(species, rpkm, fill = 0) %>%
  dplyr::rename(
    'HUM_evodevo_max' = HUM,
    'MOU_evodevo_max' = MOU
  )

# calling in v2 ----------------------------------------------------------------
out <- list(); for( dfv in list(
  list(
    'df' = dfv2,
    'name' = 'v2_only'
  ),
  list(
    'df' = dfv2v3,
    'name' = 'v2_hv3'
  )
)){
  
  df_expression_class <- dfv[['df']] %>%
    group_by(species, gene_id, cell_type, stage) %>%
    summarise(n_pb_above_cutoff = sum(value > 50),
              value = max(value)) %>%
    ungroup() %>%
    mutate(class = case_when(
      value > 50 & n_pb_above_cutoff > 1 ~ "high",
      value > 50 & n_pb_above_cutoff < 2 ~ 'noisy',
      TRUE ~ 'low'
    )) %>%
    group_by(gene_id, species, cell_type, class, stage) %>%
    dplyr::count() %>%
    group_by(gene_id, species, cell_type, class) %>%
    summarise(n = sum(n))
  
  df_trinarization <- df_expression_class %>%
    spread(class, n, fill = 0) %>%
    mutate(class = case_when(
      high > 0 ~ 'expressed',
      TRUE ~ 'background'
    )) %>%
    select(-c(high, low, noisy)) %>%
    spread(species, class)
  
  #
  # getting the first and second highest value
  tmp <- dfv[['df']]  %>%
    group_by(gene_id, species, cell_type) %>%
    summarise(cpm_max = max(value),
              cpm_second_max = sort(value, decreasing = TRUE)[2])
  
  # spreading the maximum values into a wide table
  mx <- tmp %>% 
    ungroup() %>% 
    select(-cpm_second_max) %>% 
    mutate(species = recode(species,
                            HUM = "HUM_max_cpm",
                            MOU = "MOU_max_cpm",
                            OPO = "OPO_max_cpm")) %>% 
    spread(species, cpm_max, fill = 0) 
  
  # spreading the second highest values into a wide table
  mn <- tmp %>% 
    ungroup() %>% 
    select(-cpm_max) %>% 
    mutate(species = recode(species,
                            HUM = "HUM_sec_cpm",
                            MOU = "MOU_sec_cpm",
                            OPO = "OPO_sec_cpm")) %>% 
    spread(species, cpm_second_max, fill = 0) 
  
  # joining into a summary table
  mxmn <- full_join(mx, mn)
  
  # sanity check
  stopifnot(
    all(
      mxmn$HUM_max_cpm >= mxmn$HUM_sec_cpm &
        mxmn$MOU_max_cpm >= mxmn$MOU_sec_cpm 
    )
  )
  
  # calculating the species differences using the second highest as 
  # nominator and the highest as denominator.
  df_max_express <- mxmn %>% 
    mutate(
      hm = HUM_sec_cpm / MOU_max_cpm,
      mh = MOU_sec_cpm / HUM_max_cpm
    )
  
  ## Summary statistics ----------------------------------------------------------
  df_smry_stat <- df_trinarization %>%
    ungroup() %>%
    left_join(df_max_express, by = c("gene_id", "cell_type")) %>%
    left_join(
      df_expression_class %>%
        ungroup() %>%
        mutate(class = paste(species, class, sep = '_')) %>%
        select(-species) %>%
        spread(class, n, fill = 0),
      by = c("gene_id", "cell_type")
    ) %>%
    left_join(evodevo.max) %>%
    mutate(HUM_evodevo_max = ifelse(is.na(HUM_evodevo_max), 0, HUM_evodevo_max),
           MOU_evodevo_max = ifelse(is.na(MOU_evodevo_max), 0, MOU_evodevo_max)) %>%
    group_by(gene_id) %>%
    mutate(
      HUM_is_detectable_elsewhere = sum(HUM == 'expressed') > 0,
      MOU_is_detectable_elsewhere = sum(MOU == 'expressed') > 0
    ) %>%
    ungroup() %>%
    # mutate(HUM_sc = HUM,
    #        MOU_sc = MOU,
    #        OPO_sc = OPO) %>%
    mutate(
      HUM_evodevo_sc = ifelse(HUM == 'expressed' | (HUM_evodevo_max > 0 & !HUM_is_detectable_elsewhere), 'expressed', 'background'),
      MOU_evodevo_sc = ifelse(MOU == 'expressed' | (MOU_evodevo_max > 0 & !MOU_is_detectable_elsewhere), 'expressed', 'background')
    ) %>%
    mutate(
      keep_or_remove = ifelse(
        HUM != HUM_evodevo_sc |
          MOU != MOU_evodevo_sc,
        'not analysed',
        'analysed'
      )
    )
  
  df_smry_stat %>%
    select(gene_id, keep_or_remove) %>%
    distinct() %>%
    group_by(keep_or_remove) %>%
    dplyr::count()
  
  # adding the proportion of max expression statistic ----------------------------
  prop_max <- tmp %>% 
    group_by(gene_id, species) %>% 
    mutate(cpm_tot_max = max(cpm_max) + (1/1000)) %>% 
    mutate(cpm_prop_max = cpm_max / cpm_tot_max) %>% 
    ungroup()
  
  ## classification --------------------------------------------------------------
  df_classes <- df_smry_stat  %>%
    ungroup() %>%
     left_join(prop_max %>% 
              select(gene_id, 
                     species, 
                     cell_type, 
                     cpm_prop_max) %>% 
              mutate(species = paste0(
                species, '_prop_max'
              )) %>% 
              spread(species, 
                     cpm_prop_max)) %>% 
    left_join(exon_classification) %>%
    mutate(
      class = case_when(
        keep_or_remove == 'not analysed' ~ 'not analysed',
        HUM == 'expressed' &
          MOU == 'background' & MOU_max_cpm < detection_cutoff &
          HUM_prop_max > prop_percentile_cutoff &
          MOU_prop_max < prop_percentile_cutoff &
          hm >= min_fold_difference ~ '01_Human specific/Mouse lost',
        
        HUM == 'background' & HUM_max_cpm < detection_cutoff &
          MOU == 'expressed' &
          HUM_prop_max < prop_percentile_cutoff &
          MOU_prop_max > prop_percentile_cutoff &
          mh >= min_fold_difference ~ '02_Human lost/Mouse specific',
        
        HUM == 'background' & HUM_max_cpm < detection_cutoff &
          MOU == 'background' & MOU_max_cpm < detection_cutoff ~ '09_low',
        HUM == MOU & HUM %in% c('expressed') &
          HUM_prop_max > prop_percentile_cutoff &
          MOU_prop_max > prop_percentile_cutoff ~ '07_conserved',
        TRUE ~ '08_ambiguous'
      )
    ) %>%
    mutate(class = case_when(
      exon_length_class == "comparable" ~ class,
      exon_length_class == "exclude" ~ "not analysed",
      TRUE ~ "not analysed"
    )) %>%
    left_join(gns_meta, by = 'gene_id')
  
  out[[dfv$name]] = df_classes
}

dfv2 %>% 
  select(stage, species, cell_type) %>% 
  distinct() %>% 
  group_by(cell_type, species) %>% 
  dplyr::count(name = 'stages') %>% 
  spread(species, stages)

dfhv3 %>% 
  select(stage, species, cell_type) %>% 
  distinct() %>% 
  group_by(cell_type, species) %>% 
  dplyr::count(name = 'stages') %>% 
  spread(species, stages)

clrs <- read_csv("data/metadata/color_codes_annotation.csv") %>%
  select(cell_type, cell_type_colour) %>% 
  distinct()

clrs_vec <- clrs %>% pull(cell_type_colour)
names(clrs_vec) <- clrs %>% pull(cell_type)

theme_set(theme_classic())
out_df <- lapply(names(out), function(n){
  x <- out[[n]]
  x %>% 
    group_by(cell_type, class) %>% 
    dplyr::count() %>% 
    add_column(gr = n)
}) %>% 
  do.call(rbind,.) %>% 
  spread(gr, n) %>% 
  mutate(change = (v2_hv3 / v2_only - 1) * 100) 

out_df %>% 
  write_csv("output/gain_loss_v2_v2_influence.csv")

out_df %>% 
  ggplot(aes(x = class, y = change)) +
  geom_boxplot(outlier.shape = NA) +
  geom_jitter(aes(color = cell_type),
              width = 0.15) +
  geom_hline(yintercept = 0) +
  scale_x_discrete(
    label = function(x) str_remove(x, '^.._')
  ) +
  scale_color_manual(values = clrs_vec) +
  labs(
    y = 'change [%]',
    title = 'Change in presence / absence calls',
    subtitle = 'V2 mouse and human vs. mouse [V2] and human [V2 + V3]',
    caption = 'Matches stages per cell type and chromium versions'
  ) +
  coord_flip()
ggsave('output/figures/gain_loss_v2_v3_influence.pdf',
       width = 7, height = 5)
